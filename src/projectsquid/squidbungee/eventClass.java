package projectsquid.squidbungee;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.ServerPing.Protocol;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class eventClass implements Listener {

	public boolean hasRank(ProxiedPlayer p, int rank){
		if(maps.rank.containsKey(p.getName()) && maps.rank.get(p.getName()) >= rank){
			return true;
		}
		return false;
	}
	
	@EventHandler
	public void serverPing(ProxyPingEvent e){
		ServerPing ping = new ServerPing();
		ping.setDescription(mainClass.prefix + mainClass.colorize("&cF&el&ao&9w&be&2r &fW&aa&cr&3f&aa&br&ce &6, &cSG:Magic &6and &eUndercover Murder!"));
		ping.setVersion(new Protocol(mainClass.colorize("&7" + BungeeCord.getInstance().getPlayers().size() + "&8/&75000"), 9383847));
		e.setResponse(ping);
	}
	
	@EventHandler
    public void onKick(ServerKickEvent event) {
        final ProxiedPlayer p = event.getPlayer();
        if (event.getPlayer().getServer() != null) {
            if (!event.getPlayer().getServer().getInfo().getName().equalsIgnoreCase("hub") && event.getKickReasonComponent().equals(new TextComponent("Server closed"))) {
                        p.connect(BungeeCord.getInstance().getServerInfo("hub"));
                        p.sendMessage(new TextComponent(ChatColor.RED + "Server is restarting/stopping!"));
                    }

        }
    }
	
	@EventHandler
	public void onChatEvent(ChatEvent e){
		ProxiedPlayer p = (ProxiedPlayer) e.getSender();
		if((e.getMessage().startsWith("/help") ||
		   e.getMessage().startsWith("/?") ||
		   e.getMessage().startsWith("/pl") ||
		   e.getMessage().startsWith("/plugins") ||
		   e.getMessage().startsWith("/plugin") ||
		   e.getMessage().startsWith("/bukkit") ||
		   e.getMessage().startsWith("/spigot") ||
		   e.getMessage().startsWith("/about") ||
		   e.getMessage().startsWith("/echopet") ||
		   e.getMessage().startsWith("/pet")) &&
		   !(hasRank(p, 6))){
			e.setCancelled(true);
			p.sendMessage(new TextComponent(mainClass.deniedMessage));
		}
			
		if(hasRank(p, 4) && maps.staffChat.contains(p.getName())){
			int rankId = maps.rank.get(p.getName());
			if(!e.getMessage().startsWith("/")){
				mainClass.tellStaff(ChatColor.DARK_RED + "" + ChatColor.BOLD + "STAFF ▌ " + rank.rankColors[rankId] + p.getName() + " " + ChatColor.DARK_GRAY + "» " + ChatColor.WHITE + e.getMessage() , 4);
				e.setCancelled(true);
			} 
		}
		
	}
	
	
	@EventHandler
	public void onPostLogin(PostLoginEvent e){
		final ProxiedPlayer player = e.getPlayer();
		player.connect(BungeeCord.getInstance().getServerInfo("hub"));
		if(mainClass.maintenance == true && !hasRank(player, 5)){
			player.disconnect(new TextComponent("Oiuh is down for maintenance"));
			return;
		}
		BungeeCord.getInstance().getScheduler().runAsync(mainClass.plugin, new Runnable(){
			public void run(){
				boolean banned = false;
				String uuid = player.getUniqueId().toString();
				try {
					PreparedStatement ps = sqlHandler.DatabaseObject.prepareStatement("SELECT * FROM bans WHERE uuid = ? AND unbanned = 0 ORDER BY id DESC LIMIT 1");
					ps.setString(1, uuid);
					ResultSet result = ps.executeQuery();
					if(result.next()){
						long time = result.getLong("expire");
						if(time == 0 || time > (System.currentTimeMillis() / 1000)){
							String timeToString = time == 0 ? "never" : "in " + Math.floor(((System.currentTimeMillis() / 1000) - time) / 60 / 60) + " hours";
							banned = true;
							player.disconnect(new TextComponent("You have been banned from the Oiuh Network.\nYour ban will expire " + timeToString + ".\nReason for your ban: " + result.getString("reason")));
							return;
						}
					}
					result.close();
					ps.close();
					
				} catch(SQLException exception){
					player.disconnect(new TextComponent("Database Error!"));
					exception.printStackTrace();
				}
				
				if(banned == false){
					if(maps.rank.containsKey(player.getName())){
						maps.rank.remove(player.getName());
					}
					int rank = 0;
					try {
						PreparedStatement ps = sqlHandler.DatabaseObject.prepareStatement("SELECT * FROM users WHERE uuid = ?");
						ps.setString(1, uuid);
						ResultSet result = ps.executeQuery();
						if(result.next()){
							rank = result.getInt("rank");
						} else {
							PreparedStatement pr = sqlHandler.DatabaseObject.prepareStatement("INSERT INTO users (username, uuid, rank, credits, whereami) VALUES (?, ?, 0, 1, 'Just logged in on Oiuh for the first time')");
							pr.setString(1, player.getName());
							pr.setString(2, player.getUniqueId().toString());
							pr.executeUpdate();
							pr.close();
						}
						result.close();
						ps.close();
						
					} catch(SQLException exception){
						player.disconnect(new TextComponent("Database Error!"));
						exception.printStackTrace();
					}
					maps.rank.put(player.getName(), rank);
				}
			}
		});
	}
	
	
//	@EventHandler(priority = EventPriority.HIGH)
//    public void onSwitch(ServerSwitchEvent event) {
//		final ProxiedPlayer p = event.getPlayer();
//		if(event.getPlayer().getServer().getInfo() == BungeeCord.getInstance().getServerInfo("buycraft")){
//			 BungeeCord.getInstance().getScheduler().runAsync(mainClass.plugin, new Runnable(){
//				public void run(){
//					try {
//						Thread.sleep(100);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					p.connect(BungeeCord.getInstance().getServerInfo("hub"));
//				}
//			});
//		}
//			
//	}
	

	@EventHandler(priority = EventPriority.HIGH)
	    public void onTab(TabCompleteEvent event) {
	        if (!event.getSuggestions().isEmpty()) {
	            return; //If suggestions for this command are handled by other plugin don't add any
	        }
	        String[] args = event.getCursor().split(" ");
	        String command = args[0].replace("/", "");
	        
	        if(command.equalsIgnoreCase("ban") ||
	        		command.equalsIgnoreCase("kick") ||
	        		command.equalsIgnoreCase("find") ||
	        		command.equalsIgnoreCase("note") ||
	        		command.equalsIgnoreCase("globalinfo") ||
	        		command.equalsIgnoreCase("setrank")){
	        	final String checked = (args.length > 0 ? args[args.length - 1] : event.getCursor()).toLowerCase();
	        	for (ProxiedPlayer player : BungeeCord.getInstance().getPlayers()) {
	                if (player.getName().toLowerCase().startsWith(checked)) {
	                    event.getSuggestions().add(player.getName());
	                }
	            }
	        } else if(command.equalsIgnoreCase("server") || command.equalsIgnoreCase("goto")){
	        	final String checked = (args.length > 0 ? args[args.length - 1] : event.getCursor()).toLowerCase();
	        	for (String server : BungeeCord.getInstance().getServers().keySet()) {
	                if (server.toLowerCase().startsWith(checked)) {
	                    event.getSuggestions().add(server);
	                }
	            }
	        }
	    }
	
}
