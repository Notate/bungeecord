package projectsquid.squidbungee;

import java.sql.SQLException;

import projectsquid.squidbungee.cmd.ban;
import projectsquid.squidbungee.cmd.find;
import projectsquid.squidbungee.cmd.global;
import projectsquid.squidbungee.cmd.globalinfo;
import projectsquid.squidbungee.cmd.hub;
import projectsquid.squidbungee.cmd.kick;
import projectsquid.squidbungee.cmd.list;
import projectsquid.squidbungee.cmd.note;
import projectsquid.squidbungee.cmd.reloadpayments;
import projectsquid.squidbungee.cmd.s;
import projectsquid.squidbungee.cmd.server;
import projectsquid.squidbungee.cmd.setrank;
import projectsquid.squidbungee.cmd.unban;
import projectsquid.squidbungee.cmd.maintenance;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

public class mainClass extends Plugin {
	
	public static boolean database = true;
	public final sqlHandler sqlHandler = new sqlHandler();
	public static boolean maintenance = false;
	public static String deniedMessage = ChatColor.DARK_AQUA + "[Oiuh] " + ChatColor.DARK_RED + "A squid said you are too cool for this.";
	public static String prefix = ChatColor.DARK_AQUA + "[Oiuh] ";
	public static String staffprefix = ChatColor.AQUA + "" + ChatColor.BOLD + "STAFF ▌ " + ChatColor.RED; 
	public static Plugin plugin;
	
	public static String[] ann_messages = {
			colorize("&3[Oiuh] &cWant to become a moderator? &ehttp://bit.ly/OiuhModapp"),
			colorize("&3[Oiuh] &c&lIf you find bugs, &c&lplease report the bug here: &4&lhttp://bit.ly/OiuhBugReport"),
			colorize("&3[Oiuh] &9Thank you for playing here. Want to support? Check out our store: &ehttp://store.oiuh.eu")
	};
	public static int ann_int = 0;
	
	@Override
	public void onEnable(){
		plugin = this;
		this.getProxy().getPluginManager().registerListener(this, new eventClass());
		getProxy().getPluginManager().registerCommand(this, new kick());
		getProxy().getPluginManager().registerCommand(this, new s());
		getProxy().getPluginManager().registerCommand(this, new list("glist"));
		getProxy().getPluginManager().registerCommand(this, new list("list"));
		getProxy().getPluginManager().registerCommand(this, new setrank());
		getProxy().getPluginManager().registerCommand(this, new hub("hub"));
		getProxy().getPluginManager().registerCommand(this, new hub("lobby"));
		getProxy().getPluginManager().registerCommand(this, new global("global"));
		getProxy().getPluginManager().registerCommand(this, new global("bg"));
		getProxy().getPluginManager().registerCommand(this, new find("find"));
		getProxy().getPluginManager().registerCommand(this, new find("whereis"));
		getProxy().getPluginManager().registerCommand(this, new find("whereare"));
		getProxy().getPluginManager().registerCommand(this, new globalinfo());
		getProxy().getPluginManager().registerCommand(this, new note("note"));
		getProxy().getPluginManager().registerCommand(this, new note("warning"));
		getProxy().getPluginManager().registerCommand(this, new ban());
		getProxy().getPluginManager().registerCommand(this, new unban());
		getProxy().getPluginManager().registerCommand(this, new server("server"));
		getProxy().getPluginManager().registerCommand(this, new server("goto"));
		getProxy().getPluginManager().registerCommand(this, new maintenance("maintenance"));
		getProxy().getPluginManager().registerCommand(this, new reloadpayments("payments"));
		getProxy().getPluginManager().registerCommand(this, new reloadpayments("reloadpayments"));
		sqlHandler.start();
	}
	
	@Override
	public void onDisable(){
		try {
			projectsquid.squidbungee.sqlHandler.DatabaseObject.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		projectsquid.squidbungee.sqlHandler.i.cancel();
	}
	
	public static void tellStaff(String message, int rank){
		for(ProxiedPlayer p : BungeeCord.getInstance().getPlayers()){
			if(maps.rank.containsKey(p.getName()) && maps.rank.get(p.getName()) >= rank){
				p.sendMessage(new TextComponent(message));
			}
		}
			
	}
	
	public static String colorize(String string) {
	    string = string.replaceAll("&0", ChatColor.BLACK+"");
	    string = string.replaceAll("&1", ChatColor.DARK_BLUE+"");
	    string = string.replaceAll("&2", ChatColor.DARK_GREEN+"");
	    string = string.replaceAll("&3", ChatColor.DARK_AQUA+"");
	    string = string.replaceAll("&4", ChatColor.DARK_RED+"");
	    string = string.replaceAll("&5", ChatColor.DARK_PURPLE+"");
	    string = string.replaceAll("&6", ChatColor.GOLD+"");
	    string = string.replaceAll("&7", ChatColor.GRAY+"");
	    string = string.replaceAll("&8", ChatColor.DARK_GRAY+"");
	    string = string.replaceAll("&9", ChatColor.BLUE+"");
	    string = string.replaceAll("&a", ChatColor.GREEN+"");
	    string = string.replaceAll("&b", ChatColor.AQUA+"");
	    string = string.replaceAll("&c", ChatColor.RED+"");
	    string = string.replaceAll("&d", ChatColor.LIGHT_PURPLE+"");
	    string = string.replaceAll("&e", ChatColor.YELLOW+"");
	    string = string.replaceAll("&f", ChatColor.WHITE+"");
	    string = string.replaceAll("&l", ChatColor.BOLD+"");
	    string = string.replaceAll("&n", ChatColor.UNDERLINE+"");
	    string = string.replaceAll("&i", ChatColor.ITALIC+"");
	    string = string.replaceAll("&r", ChatColor.RESET+"");
	    return string;
	}
}
