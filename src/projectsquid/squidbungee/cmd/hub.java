package projectsquid.squidbungee.cmd;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class hub extends Command {

	public hub(String cmd){
		super(cmd);
	}

	@Override
	public void execute(CommandSender arg0, String[] arg1) {
		((ProxiedPlayer) arg0).connect(ProxyServer.getInstance().getServerInfo("hub"));
		
	}
	
}
