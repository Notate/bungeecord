package projectsquid.squidbungee.cmd;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import projectsquid.squidbungee.mainClass;
import projectsquid.squidbungee.maps;
import projectsquid.squidbungee.rank;
import projectsquid.squidbungee.sqlHandler;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class globalinfo extends Command {

	public globalinfo(){
		super("globalinfo");
	}
	
	public boolean hasRank(ProxiedPlayer p, int rank){
		if(maps.rank.containsKey(p.getName()) && maps.rank.get(p.getName()) >= rank){
			return true;
		}
		return false;
	}

	@Override
	public void execute(CommandSender commandSender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) commandSender;
		if(hasRank(p, 4)){
			if(args.length == 0){
				p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Usage: /globalinfo <player> [notes]"));
			} else if(args.length >= 1){
				String uuidForPlayer = null;
				String username = args[0];
				int rankid = 0;
				boolean playerexist = false;
				try {
					PreparedStatement getUserdata = sqlHandler.DatabaseObject.prepareStatement("SELECT * FROM users WHERE username = ?");
					getUserdata.setString(1, username);
					ResultSet rs = getUserdata.executeQuery();
					if(rs.next()){
						playerexist = true;
						rankid = rs.getInt("rank");
						uuidForPlayer = rs.getString("uuid");
					} 
					rs.close();
					getUserdata.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
				if(playerexist == false){
					p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Player doesn't exist!"));
				} else {
					if(args.length == 1){
						
						boolean banned = false;
						String reason = "";
						try {
							PreparedStatement ps = sqlHandler.DatabaseObject.prepareStatement("SELECT * FROM bans WHERE uuid = ? AND (expire = 0 OR expire > UNIX_TIMESTAMP()) AND unbanned = 0");
							ps.setString(1, uuidForPlayer);
							ResultSet rs = ps.executeQuery();
							if(rs.next()){
								banned = true;
								reason = rs.getString("reason");
							}
							rs.close();
							ps.close();
						} catch(SQLException e){
							p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Seems that a squid has broken the database."));
							e.printStackTrace();
						}
						
						
						String status = BungeeCord.getInstance().getPlayer(username) != null ? ChatColor.GREEN + "Online" : ChatColor.DARK_RED + "Offline";
						p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + "Player Information about " + username));
						p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + ChatColor.BOLD + "------------------------------------------"));
						p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + "Username: " + ChatColor.DARK_PURPLE + username));
						p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + "UUID: " + ChatColor.DARK_PURPLE + uuidForPlayer));
						p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + "Rank: " + ChatColor.DARK_PURPLE + rank.rankNames[rankid]));
						p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + "Status: " + status));
						p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + "Banned: " + ChatColor.DARK_PURPLE + banned));
						if(banned == true){
							p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + "Reason for ban: " + ChatColor.DARK_PURPLE + reason));
						}
						ProxiedPlayer t = BungeeCord.getInstance().getPlayer(username);
						if(t != null){
							p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + "Server: " + ChatColor.DARK_PURPLE + t.getServer().getInfo().getName()));
							p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + "IP: " + ChatColor.DARK_PURPLE + t.getAddress().getHostString()));
						}
					} else {
						if(args[1].equalsIgnoreCase("notes")){
							p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + "Note Information about " + username));
							p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + ChatColor.BOLD + "------------------------------------------"));
							try {
								PreparedStatement getNotedata = sqlHandler.DatabaseObject.prepareStatement(
										"SELECT notes.*, userdata.username FROM notes AS notes INNER JOIN users AS userdata ON notes.givenby = userdata.uuid WHERE notes.givento = ? ORDER BY notes.id DESC");
								getNotedata.setString(1, uuidForPlayer);
								ResultSet rs = getNotedata.executeQuery();
								SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.YY HH:mm:ss");
								int i = 0;
								while(rs.next()){
									java.util.Date resultDate=new java.util.Date(((long)rs.getInt("given")*1000) - (3600*2));
									p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + "Given by " + ChatColor.DARK_PURPLE + rs.getString("username") + ChatColor.YELLOW + ", time "
											+ "" + ChatColor.DARK_PURPLE + sdf.format(resultDate) + ChatColor.YELLOW + ". Server " + ChatColor.DARK_PURPLE + rs.getString("server")));
									p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + "Reason: " + ChatColor.GRAY + rs.getString("reason")));
									p.sendMessage(new TextComponent(""));
									i++;
								}
								if(i == 0){
									p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + "Player doesn't have any notes"));
								}
								rs.close();
								getNotedata.close();
							} catch(SQLException e){
								p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + "The database is broken"));
							}
						}
					}
				}
			}
		} else {
			p.sendMessage(new TextComponent(mainClass.deniedMessage));
		}
		
	}
	
}
