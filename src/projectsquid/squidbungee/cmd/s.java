package projectsquid.squidbungee.cmd;

import projectsquid.squidbungee.mainClass;
import projectsquid.squidbungee.maps;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class s extends Command {

	public s(){
		super("s");
	}
	
	public boolean hasRank(ProxiedPlayer p, int rank){
		if(maps.rank.containsKey(p.getName()) && maps.rank.get(p.getName()) >= rank){
			return true;
		}
		return false;
	}

	@Override
	public void execute(CommandSender commandSender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) commandSender;
		if(hasRank(p, 4)){
			if(maps.staffChat.contains(p.getName())){
				maps.staffChat.remove(p.getName());
				p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.GREEN + "You are no longer participating in the staffchat."));
			} else {
				maps.staffChat.add(p.getName());
				p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.GREEN + "You are now participating in the staffchat."));
			}
		} else {
			p.sendMessage(new TextComponent(mainClass.deniedMessage));
		}
		
	}
	
}
