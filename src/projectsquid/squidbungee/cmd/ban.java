package projectsquid.squidbungee.cmd;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import projectsquid.squidbungee.mainClass;
import projectsquid.squidbungee.maps;
import projectsquid.squidbungee.sqlHandler;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ban extends Command {

	public ban() {
		super("ban");
	}

	public boolean hasRank(ProxiedPlayer p, int rank){
		if(maps.rank.containsKey(p.getName()) && maps.rank.get(p.getName()) >= rank){
			return true;
		}
		return false;
	}
	
	@Override
	public void execute(CommandSender commandSender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) commandSender;
		if(hasRank(p, 4)){
			if(args.length <= 1){
				p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Usage: /ban <player> <days|perm> <reason>"));
			} else {
				boolean ex = false;
				String uuid = null;
				try {
					PreparedStatement ps = sqlHandler.DatabaseObject.prepareStatement("SELECT * FROM users WHERE username = ?");
					ps.setString(1, args[0]);
					ResultSet rs = ps.executeQuery();
					if(rs.next()){
						ex = true;
						uuid = rs.getString("uuid");
					}
					rs.close();
					ps.close();
				} catch (SQLException e) {
					p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Seems that a squid has broken the database."));
					e.printStackTrace();
				}
				
				if(ex == true){
					String reason = "";
					for(int i = 2; i < args.length; i++){
						reason += args[i] + " ";
					}
					long time = 0;
					if(args[1].equalsIgnoreCase("perm")){
						time = 0;
					} else {
						time = System.currentTimeMillis() / 1000 + (Integer.parseInt(args[1]) * 60 * 60 * 24);
					}
					
					try {
						PreparedStatement ps = sqlHandler.DatabaseObject.prepareStatement("INSERT INTO bans (uuid, reason, expire, unbanned)"
								+ " VALUES(?, ?, ?, 0)");
						ps.setString(1, uuid);
						ps.setString(2, reason);
						ps.setLong(3, time);
						ps.executeUpdate();
						if(BungeeCord.getInstance().getPlayer(args[0]) != null){
							BungeeCord.getInstance().getPlayer(args[0]).disconnect(new TextComponent("You've been banned from the network: " + reason));
						}
						mainClass.tellStaff(mainClass.staffprefix + args[0] + " was banned by " + p.getName() + " for " + reason, 4);
					} catch(SQLException e){
						e.printStackTrace();
						p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Seems that a squid has broken the database."));
					}
				} else {
					p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Player doesn't exist!"));
				}
			}
		} else {
			p.sendMessage(new TextComponent(mainClass.deniedMessage));
		}
	}

	
	
}
