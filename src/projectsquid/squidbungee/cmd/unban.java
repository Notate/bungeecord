package projectsquid.squidbungee.cmd;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import projectsquid.squidbungee.mainClass;
import projectsquid.squidbungee.maps;
import projectsquid.squidbungee.sqlHandler;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class unban extends Command {

	public unban() {
		super("unban");
	}

	public boolean hasRank(ProxiedPlayer p, int rank){
		if(maps.rank.containsKey(p.getName()) && maps.rank.get(p.getName()) >= rank){
			return true;
		}
		return false;
	}
	
	@Override
	public void execute(CommandSender commandSender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) commandSender;
		if(hasRank(p, 4)){
			if(args.length == 0){
				p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Usage: /unban <player>"));
			} else {
				boolean ex = false;
				String uuid = null;
				try {
					PreparedStatement ps = sqlHandler.DatabaseObject.prepareStatement("SELECT * FROM users WHERE username = ?");
					ps.setString(1, args[0]);
					ResultSet rs = ps.executeQuery();
					if(rs.next()){
						ex = true;
						uuid = rs.getString("uuid");
					}
					rs.close();
					ps.close();
				} catch (SQLException e) {
					p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Seems that a squid has broken the database."));
					e.printStackTrace();
				}
				
				if(ex == true){
					boolean banned = false;
					try {
						PreparedStatement ps = sqlHandler.DatabaseObject.prepareStatement("SELECT * FROM bans WHERE uuid = ? AND (expire = 0 OR expire > UNIX_TIMESTAMP()) AND unbanned = 0");
						ps.setString(1, uuid);
						ResultSet rs = ps.executeQuery();
						if(rs.next()){
							banned = true;
						}
						rs.close();
						ps.close();
					} catch(SQLException e){
						p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Seems that a squid has broken the database."));
						e.printStackTrace();
					}
					
					if(banned == true){
						try {
							PreparedStatement ps = sqlHandler.DatabaseObject.prepareStatement("UPDATE bans SET unbanned = 1 WHERE uuid = ?");
							ps.setString(1, uuid);
							ps.executeUpdate();
							mainClass.tellStaff(mainClass.staffprefix + args[0] + " was UNbanned by " + p.getName(), 4);
							ps.close();
						} catch(SQLException e){
							p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Seems that a squid has broken the database."));
							e.printStackTrace();
						}
					} else {
						p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Player is not banned!"));
					}
				} else {
					p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Player doesn't exist!"));
				}
			}
		} else {
			p.sendMessage(new TextComponent(mainClass.deniedMessage));
		}
	}

	
	
}
