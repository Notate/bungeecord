package projectsquid.squidbungee.cmd;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import projectsquid.squidbungee.mainClass;
import projectsquid.squidbungee.maps;
import projectsquid.squidbungee.sqlHandler;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class setrank extends Command {

	public setrank(){
		super("setrank");
	}
	
	public boolean hasRank(ProxiedPlayer p, int rank){
		if(maps.rank.containsKey(p.getName()) && maps.rank.get(p.getName()) >= rank){
			return true;
		}
		return false;
	}

	@Override
	public void execute(CommandSender commandSender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) commandSender;
		if(hasRank(p, 6)){
			if(args.length <= 1){
				p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Usage: /setrank <player> <0-7>"));
			} else {
				boolean ex = false;
				try {
					PreparedStatement ps = sqlHandler.DatabaseObject.prepareStatement("SELECT * FROM users WHERE username = ?");
					ps.setString(1, args[0]);
					ResultSet rs = ps.executeQuery();
					if(rs.next()){
						ex = true;
					}
					rs.close();
					ps.close();
				} catch (SQLException e) {
					p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Seems that a squid has broken the database."));
					e.printStackTrace();
				}
				
				if(ex == true){
					try {
						PreparedStatement ps = sqlHandler.DatabaseObject.prepareStatement("UPDATE users SET rank = ? WHERE username = ?");
						ps.setInt(1, Integer.parseInt(args[1]));
						ps.setString(2, args[0]);
						ps.executeUpdate();
						p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.GREEN + args[0] + "'s has been updated to " + args[1]));
						ps.close();
					} catch (SQLException e) {
						p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Seems that a squid has broken the database."));
						e.printStackTrace();
					}
					
				} else {
					p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Player doesn't exist!"));
				}
			}
		} else {
			p.sendMessage(new TextComponent(mainClass.deniedMessage));
		}
		
	}
	
}
