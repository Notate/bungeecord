package projectsquid.squidbungee.cmd;

import projectsquid.squidbungee.mainClass;
import projectsquid.squidbungee.maps;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class kick extends Command {

	public kick(){
		super("kick");
	}
	
	public boolean hasRank(ProxiedPlayer p, int rank){
		if(maps.rank.containsKey(p.getName()) && maps.rank.get(p.getName()) >= rank){
			return true;
		}
		return false;
	}
	
	public void execute(CommandSender commandSender, String[] args) {
	
		ProxiedPlayer p = (ProxiedPlayer) commandSender;
		if(hasRank(p, 4)){
			if(args.length == 0){
				p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Usage: /kick <player> [reason]"));
			} else {
				ProxiedPlayer t = BungeeCord.getInstance().getPlayer(args[0]);
				if(t != null){
					String reason = "You've been kicked by a staffmember.";
					if(args.length > 1){
						reason = "";
						for(int i = 1; i < args.length; i++){
							reason += args[i] + " ";
						}
					}
					t.disconnect(new TextComponent(reason));
					mainClass.tellStaff(mainClass.staffprefix + t.getName() + " was kicked by " + p.getName() + " for " + reason, 4);
				} else {
					p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Player is not online at the moment."));
				}
			}
		} else {
			p.sendMessage(new TextComponent(mainClass.deniedMessage));
		}
		
	}
	  
}
