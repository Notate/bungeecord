package projectsquid.squidbungee.cmd;

import projectsquid.squidbungee.mainClass;
import projectsquid.squidbungee.maps;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class find extends Command {

	public find(String cmd){
		super(cmd);
	}

	public boolean hasRank(ProxiedPlayer p, int rank){
		if(maps.rank.containsKey(p.getName()) && maps.rank.get(p.getName()) >= rank){
			return true;
		}
		return false;
	}
	
	@Override
	public void execute(CommandSender commandSender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) commandSender;
		if(hasRank(p, 4)){
			if(args.length == 0){
				p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Usage: /find <player>"));
			} else {
				ProxiedPlayer t = BungeeCord.getInstance().getPlayer(args[0]);
				if(t != null){
					p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.GREEN + t.getName() + " is on the server " + ChatColor.DARK_PURPLE + t.getServer().getInfo().getName()));
				} else {
					p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Player is not online at the moment."));
				}
			}
		} else {
			p.sendMessage(new TextComponent(mainClass.deniedMessage));
		}
		
	}
	
}
