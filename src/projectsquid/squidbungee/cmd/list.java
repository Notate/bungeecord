package projectsquid.squidbungee.cmd;

import projectsquid.squidbungee.mainClass;
import projectsquid.squidbungee.maps;
import projectsquid.squidbungee.rank;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class list extends Command {

	public list(String command){
		super(command);
	}

	@Override
	public void execute(CommandSender commandSender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) commandSender;
		int inyourserver = 0;
		String inyourserverInString = "";
		for(ProxiedPlayer p2 : BungeeCord.getInstance().getPlayers()){
			if(p2.getServer().getInfo().getName() == p.getServer().getInfo().getName()){
				inyourserver++;
				inyourserverInString += rank.rankColors[maps.rank.get(p2.getName())] + p2.getName() + ", ";
			}
		}
		p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + "There are " + ChatColor.DARK_PURPLE + inyourserver + ChatColor.YELLOW + " players at this server."));
		p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.YELLOW + "List: " + inyourserverInString));
		
	}
	
	
	
	
}
