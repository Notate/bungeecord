package projectsquid.squidbungee.cmd;

import projectsquid.squidbungee.mainClass;
import projectsquid.squidbungee.maps;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.scheduler.ScheduledTask;

public class maintenance extends Command {

	public maintenance(String name) {
		super(name);
	}
	
	public ScheduledTask latest = null;
	public boolean maintenance = false;

	public boolean hasRank(ProxiedPlayer p, int rank){
		if(maps.rank.containsKey(p.getName()) && maps.rank.get(p.getName()) >= rank){
			return true;
		}
		return false;
	}
	
	@Override
	public void execute(CommandSender commandSender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) commandSender;
		if(hasRank(p, 7)){
		 if(mainClass.maintenance == true){
			 mainClass.maintenance = false;
			 p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "Maintenance mode is now off!"));
			 return;
		 } 
		 
		 if(latest != null){
			 p.sendMessage(new TextComponent(mainClass.prefix + ChatColor.DARK_RED + "The scheduled maintenance mode is now cancelled!"));
			 latest.cancel();
			 latest = null;
			 return;
		 }
		 
		 if(maintenance == true){
			 maintenance = false;
			 return;
		 }
		 
			latest = BungeeCord.getInstance().getScheduler().runAsync(mainClass.plugin, new Runnable(){
				public void run(){
					setMaintenance(true);
					int minutes = 10;
					while(maintenance == true){
						if(minutes == 0){
							maintenance = false;
							mainClass.maintenance = true;
							for(ProxiedPlayer p2 : BungeeCord.getInstance().getPlayers()){
								p2.disconnect(new TextComponent("Server went down for maintenance mode."));
							}
						} else {
							mainClass.tellStaff(mainClass.prefix + ChatColor.DARK_RED + "The server is going in maintenance mode in " + minutes + " minutes!", 0);
							
							try {
								Thread.sleep(60000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						minutes--;
						}
					}
				}
			});
		} else {
			p.sendMessage(new TextComponent(mainClass.deniedMessage));
		}
		
	}
	
	public void setMaintenance(boolean b){
		maintenance = b;
	}

}
