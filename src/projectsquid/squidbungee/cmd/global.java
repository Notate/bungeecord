package projectsquid.squidbungee.cmd;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import projectsquid.squidbungee.mainClass;
import projectsquid.squidbungee.maps;

public class global extends Command {

	public global(String cmd) {
		super(cmd);
	}

	public boolean hasRank(ProxiedPlayer p, int rank){
		if(maps.rank.containsKey(p.getName()) && maps.rank.get(p.getName()) >= rank){
			return true;
		}
		return false;
	}
	
	@Override
	public void execute(CommandSender commandSender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) commandSender;
		if(hasRank(p, 6)){
			String text = "";
			for(int i = 0; i < args.length; i++){
				text += args[i] + " ";
			}
			
			mainClass.tellStaff(mainClass.colorize("&6[Global] &2" + p.getName() + " &6| &2" + text), 0);
		} else {
			p.sendMessage(new TextComponent(mainClass.deniedMessage));
		}
		
	}
	

}
